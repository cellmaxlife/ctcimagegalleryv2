#include "stdafx.h"
#include "ColorStain.h"

CColorStain::CColorStain()
{
	m_RedRgnPtr = NULL;
	m_RedBlob = NULL;
	m_GreenBlob = NULL;
}

CColorStain::~CColorStain()
{
	if (m_RedRgnPtr != NULL)
	{
		delete m_RedRgnPtr;
		m_RedRgnPtr = NULL;
	}
	if (m_RedBlob != NULL)
	{
		delete m_RedBlob;
		m_RedBlob = NULL;
	}
	if (m_GreenBlob != NULL)
	{
		delete m_GreenBlob;
		m_GreenBlob = NULL;
	}
	for (int i = 0; i < (int)m_BlueBlobs.size(); i++)
	{
		if (m_BlueBlobs[i] != NULL)
		{
			delete m_BlueBlobs[i];
			m_BlueBlobs[i] = NULL;
		}
	}
	m_BlueBlobs.clear();
	
}