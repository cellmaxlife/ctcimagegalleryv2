#include "stdafx.h"
#include "MSERDetector.h"

CMSERDetector::CMSERDetector()
{
}

CMSERDetector::~CMSERDetector()
{
}

template<typename T>
void CMSERDetector::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

template<typename T>
int CMSERDetector::Partition(vector<T> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	unsigned short intensity = (*list)[position]->intensity;
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->intensity < intensity)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

template<typename T>
int CMSERDetector::ChoosePivot(vector<T> *list, int lo, int hi)
{
	int pivot = lo;
	unsigned short intensity0 = (*list)[lo]->intensity;
	unsigned short intensity1 = (*list)[(lo + hi) / 2]->intensity;
	unsigned short intensity2 = (*list)[hi]->intensity;
	if ((intensity0 <= intensity1) && (intensity1 <= intensity2))
		pivot = (lo + hi) / 2;
	else if ((intensity0 <= intensity2) && (intensity2 <= intensity1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
};

template<typename T>
void CMSERDetector::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

bool CMSERDetector::IsArtifact(INTENSITY_PIXEL_SET *intensityPixelSet, int width, int height)
{
	bool ret = false;
	int minX = width;
	int maxX = 0;
	int minY = height;
	int maxY = 0;

	for (int i = 0; i < (int)intensityPixelSet->pixelList->size(); i++)
	{
		int x = (*intensityPixelSet->pixelList)[i] % width;
		int y = (*intensityPixelSet->pixelList)[i] / width;
		if (x < minX)
			minX = x;
		if (x > maxX)
			maxX = x;
		if (y < minY)
			minY = y;
		if (y > maxY)
			maxY = y;
	}
	double area = (double) (maxX - minX) * (maxY * minY);
	double density = (double)intensityPixelSet->pixelList->size() / area;
	if (density > 0.8)
	{
		ret = true;
	}
	return ret;
}

void CMSERDetector::GetRedBlobs(unsigned short *image, int width, int height, int minCount, int maxCount, int saturateCount, int intensityDelta, vector<CBlobData *> *blobList)
{
	vector<INTENSITY_PIXEL_SET *> intensityList;
	unsigned short *imgPtr = image;
	unsigned int pixel = 0;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, imgPtr++, pixel++)
		{
			unsigned short intensity = *imgPtr;
			bool found = false;
			for (int k = 0; k < (int)intensityList.size(); k++)
			{
				if (intensityList[k]->intensity == intensity)
				{
					intensityList[k]->pixelList->push_back(pixel);
					found = true;
					break;
				}
			}
			if (!found)
			{
				INTENSITY_PIXEL_SET *ptr = new INTENSITY_PIXEL_SET(intensity, pixel);
				intensityList.push_back(ptr);
			}
		}
	}
	QuickSort(&intensityList, 0, ((int)intensityList.size()-1));
	
	vector<REGIONDATASET *> **regions;
	regions = new vector<REGIONDATASET *> *[intensityList.size()];
	memset(regions, 0, sizeof(vector<REGIONDATASET *> *) * intensityList.size());
	unsigned char *placedMap = new unsigned char[width * height];
	memset(placedMap, 0, sizeof(unsigned char) * width * height);
	int *regionMap = new int[width * height];
	memset(regionMap, 0, sizeof(int) * width * height);
	unsigned short threshold = 0;
	int maxPosition = width * height;
	int topIndex = (int)intensityList.size() - 1;
	int bottomIndex = 0;
	int totalPixelCount = 0;
	for (int i = topIndex; i > bottomIndex; i--)
	{
		totalPixelCount += (int) intensityList[i]->pixelList->size();
		if (totalPixelCount >= saturateCount) 
		{
			bottomIndex = i;
			break;
		}
		else if ((int)intensityList[i]->pixelList->size() > minCount)
		{
			if (IsArtifact(intensityList[i], width, height))
				continue;
			else if (i >= (topIndex - 1))
				continue;
			else
			{
				bottomIndex = i;
				break;
			}
		}
	}
	try
	{
		int regionReference = 0;
		for (int i = topIndex; i > bottomIndex; i--)
		{
			INTENSITY_PIXEL_SET *intenPtr = intensityList[i];
			threshold = intenPtr->intensity;
			regions[i] = new vector<REGIONDATASET *>();
			if (i < topIndex)
			{
				for (int j = 0; j < (int)regions[i + 1]->size(); j++)
				{
					REGIONDATASET *region = new REGIONDATASET((*regions[i + 1])[j], threshold);
					(*regions[i + 1])[j]->child = region;
					regions[i]->push_back(region);
				}
			}

			for (int j = 0; j < (int)intenPtr->pixelList->size(); j++)
			{
				int position = (int)(*intenPtr->pixelList)[j];
				placedMap[position] = (unsigned char)1;
				int regionIndex1 = -1;
				int regionIndexPosition = 0;
				if (((position - 1) >= 0) && (placedMap[position - 1] == (unsigned char)1))
				{
					regionIndex1 = Find((position - 1), regionMap, &regionIndexPosition, maxPosition);
					if (regionIndex1 > -1)
					{
						regionMap[position] = regionIndexPosition;
						(*regions[i])[regionIndex1]->pixels->push_back(position);
					}
				}
				if (((position + 1) < maxPosition) && (placedMap[position + 1] == (unsigned char)1))
				{
					if (regionIndex1 > -1)
					{
						regionIndexPosition = 0;
						int regionIndex2 = Find((position + 1), regionMap, &regionIndexPosition, maxPosition);
						if ((regionIndex2 > -1) && (regionIndex2 != regionIndex1))
						{
							Union(regionIndex1, regionIndex2, regions, i, topIndex, regionMap, maxPosition);
							regionIndex1 = Find(position + 1, regionMap, &regionIndexPosition, maxPosition);
						}
					}
					else
					{
						regionIndex1 = Find((position + 1), regionMap, &regionIndexPosition, maxPosition);
						if (regionIndex1 > -1)
						{
							regionMap[position] = regionIndexPosition;
							(*regions[i])[regionIndex1]->pixels->push_back(position);
						}
					}
				}
				if (((position - width) >= 0) && (placedMap[position - width] == (unsigned char)1))
				{
					if (regionIndex1 > -1)
					{
						regionIndexPosition = 0;
						int regionIndex2 = Find((position - width), regionMap, &regionIndexPosition, maxPosition);
						if ((regionIndex2 > -1) && (regionIndex2 != regionIndex1))
						{
							Union(regionIndex1, regionIndex2, regions, i, topIndex, regionMap, maxPosition);
							regionIndex1 = Find(position - width, regionMap, &regionIndexPosition, maxPosition);
						}
					}
					else
					{
						regionIndex1 = Find((position - width), regionMap, &regionIndexPosition, maxPosition);
						if (regionIndex1 > -1)
						{
							regionMap[position] = regionIndexPosition;
							(*regions[i])[regionIndex1]->pixels->push_back(position);
						}
					}
				}
				if (((position + width) < maxPosition) && (placedMap[position + width] == (unsigned char)1))
				{
					if (regionIndex1 > -1)
					{
						regionIndexPosition = 0;
						int regionIndex2 = Find((position + width), regionMap, &regionIndexPosition, maxPosition);
						if ((regionIndex2 > -1) && (regionIndex2 != regionIndex1))
						{
							Union(regionIndex1, regionIndex2, regions, i, topIndex, regionMap, maxPosition);
							regionIndex1 = Find(position + width, regionMap, &regionIndexPosition, maxPosition);
						}
					}
					else
					{
						regionIndex1 = Find((position + width), regionMap, &regionIndexPosition, maxPosition);
						if (regionIndex1 > -1)
						{
							regionMap[position] = regionIndexPosition;
							(*regions[i])[regionIndex1]->pixels->push_back(position);
						}
					}
				}
				if (regionIndex1 == -1)
				{
					regionReference++;
					REGIONDATASET *region = new REGIONDATASET(regionReference, position, threshold);
					regions[i]->push_back(region);
					int regionIndex = (int) regions[i]->size();
					regionMap[position] = -regionIndex;
				}
			}
		}
	}
	catch (...)
	{
		TRACE(_T("Exception Throwed in Find()"));
	}

	for (int i = topIndex; i >= 0; i--)
		delete intensityList[i];
	intensityList.clear();

	delete[] placedMap;
	delete[] regionMap;

	Detect(regions, topIndex, bottomIndex, blobList, image, width, height, minCount, maxCount, intensityDelta);

	for (int index = topIndex; index >= 0; index--)
	{
		if (regions[index] != NULL)
		{
			for (int regionIndex = 0; regionIndex < (int)regions[index]->size(); regionIndex++)
			{
				delete (*regions[index])[regionIndex];
				(*regions[index])[regionIndex] = NULL;
			}
			regions[index]->clear();
			delete regions[index];
			regions[index] = NULL;
		}
	}

	delete[] regions;
}

int CMSERDetector::Find(int position, int *regionMap, int *regionLocation, int maxPosition)
{
	int result = regionMap[position];
	if (result < 0)
	{
		*regionLocation = position + 1;
		result = -result;
		result--;
	}
	else if (result > 0)
	{
		*regionLocation = result;
		int regionIndexPosition = result - 1;
		if ((regionIndexPosition >= 0) && (regionIndexPosition < maxPosition))
		{
			result = regionMap[regionIndexPosition];
			if (result < 0)
			{
				result = -result;
				result--;
			}
			else
				throw 253;
		}
	}
	else
		throw 254;

	return result;
}

void CMSERDetector::Union(int regionIndex1, int regionIndex2, vector<REGIONDATASET *> *regions[], int index, int topIndex, int *regionMap, int maxPosition)
{
	int bigRegionIndex = regionIndex1;
	int smallRegionIndex = regionIndex2;
	vector<REGIONDATASET *> *rgnPtr = regions[index];
	if ((*regions[index])[regionIndex2]->pixels->size() > (*regions[index])[regionIndex1]->pixels->size())
	{
		bigRegionIndex = regionIndex2;
		smallRegionIndex = regionIndex1;
	}

	if ((*regions[index])[bigRegionIndex]->reference > (*regions[index])[smallRegionIndex]->reference)
		(*regions[index])[bigRegionIndex]->reference = (*regions[index])[smallRegionIndex]->reference;

	int regionReference = (*regions[index])[bigRegionIndex]->reference;

	for (int i = 0; i < (int)(*regions[index])[smallRegionIndex]->pixels->size(); i++)
		(*regions[index])[bigRegionIndex]->pixels->push_back((*(*regions[index])[smallRegionIndex]->pixels)[i]);
	if (index < topIndex)
	{
		for (int i = 0; i < (int)regions[index + 1]->size(); i++)
		{
			if ((*regions[index + 1])[i]->child == (*regions[index])[smallRegionIndex])
				(*regions[index + 1])[i]->child = (*regions[index])[bigRegionIndex];
		}
	}
	delete (*regions[index])[smallRegionIndex];
	(*regions[index])[smallRegionIndex] = NULL;
	regions[index]->erase(regions[index]->begin() + smallRegionIndex);
	for (int i = 0; i < (int)regions[index]->size(); i++)
	{
		int newPosition = (*regions[index])[i]->position;
		regionMap[newPosition] = -(i + 1);
		if (regionReference == (*regions[index])[i]->reference)
		{
			for (int j = 0; j < (int)(*regions[index])[i]->pixels->size(); j++)
			{
				int position = (*(*regions[index])[i]->pixels)[j];
				if (position != newPosition)
					regionMap[position] = newPosition + 1;
			}
		}
	}
}

int CMSERDetector::GetSampleIndex(vector<REGIONDATASET *> singleRegion, int intensity, int intensityDelta)
{
	int minValue = MAXUINT16;
	int minIndex = -1;
	for (int i = 0; i < (int)singleRegion.size(); i++)
	{
		int difference = abs(intensity - singleRegion[i]->intensity);
		if (difference < minValue)
		{
			minValue = difference;
			minIndex = i;
		}
	}

	if (minValue > (intensityDelta/2))
		return -1;
	else
		return minIndex;
}

bool CMSERDetector::HasBeenVisisted(unsigned short reference, vector<unsigned short> visited)
{
	bool isVisited = false;
	for (int i = 0; i < (int)visited.size(); i++)
	{
		if (reference == visited[i])
		{
			isVisited = true;
			break;
		}
	}
	return isVisited;
}

void CMSERDetector::Detect(vector<REGIONDATASET *> *regions[], int topIndex, int bottomIndex, vector<CBlobData *> *blobList, 
	unsigned short *image, int width, int height, int minCount, int maxCount, int intensityDelta)
{
	int MIN_INTENSITY_RANGE = 15;
	vector<BLOBCANDIDATE *> candidates;
	vector<unsigned short> visited;
	for (int index = topIndex; index > bottomIndex; index--)
	{
		for (int regionIndex = 0; regionIndex < (int)regions[index]->size(); regionIndex++)
		{
			if (!HasBeenVisisted((*regions[index])[regionIndex]->reference, visited))
			{
				int reference = (*regions[index])[regionIndex]->reference;
				visited.push_back(reference);
				vector<REGIONDATASET *> singleRegion;
				REGIONDATASET *ptr = (*regions[index])[regionIndex];
				if (ptr != NULL)
				{
					int blobSize = (int)ptr->pixels->size();
					if ((blobSize >= minCount) && (blobSize <= maxCount))
						singleRegion.push_back(ptr);
				}
				while ((ptr != NULL) && (ptr->reference == reference))
				{
					ptr = ptr->child;
					if (ptr != NULL)
					{
						int blobSize = (int)ptr->pixels->size();
						if ((blobSize >= minCount) && (blobSize <= maxCount))
							singleRegion.push_back(ptr);
					}
				}

				int intensityRange = 0;
				if (singleRegion.size() > 0)
					intensityRange = singleRegion[0]->intensity - singleRegion[singleRegion.size() - 1]->intensity;

				if (intensityRange > MIN_INTENSITY_RANGE)
				{
					vector<REGIONSAMPLE> samples;
					for (int k = 1; k < ((int)singleRegion.size() - 1); k++)
					{
						int middleIntensity = singleRegion[k]->intensity;
						int highIntensity = middleIntensity + intensityDelta;
						int lowIntensity = middleIntensity - intensityDelta;
						int highIndex = GetSampleIndex(singleRegion, highIntensity, intensityDelta);
						int lowIndex = GetSampleIndex(singleRegion, lowIntensity, intensityDelta);
						if ((highIndex > -1) && (lowIndex > -1))
						{
							int middleCount = (int)singleRegion[k]->pixels->size();
							int highCount = (int)singleRegion[highIndex]->pixels->size();
							int lowCount = (int)singleRegion[lowIndex]->pixels->size();
							REGIONSAMPLE sample;
							sample.sampleIndex = k;
							sample.value = abs((double)(lowCount - highCount) / (double)middleCount);
							samples.push_back(sample);
						}
					}
					double minValue = (double)MAXINT;
					int minIndex = -1;
					for (int k = 0; k < (int)samples.size(); k++)
					{
						if (samples[k].value < minValue)
						{
							minValue = samples[k].value;
							minIndex = k;
						}
					}

					if (minIndex > -1)
					{
						BLOBCANDIDATE *regionCandidate = new BLOBCANDIDATE(intensityRange, singleRegion[samples[minIndex].sampleIndex]->intensity);
						for (int k = 0; k < (int)singleRegion[samples[minIndex].sampleIndex]->pixels->size(); k++)
						{
							regionCandidate->pixels->push_back((*singleRegion[samples[minIndex].sampleIndex]->pixels)[k]);
						}
						candidates.push_back(regionCandidate);
					}

					samples.clear();
				}

				for (int i=0; i<(int)singleRegion.size(); i++)
					singleRegion[i]= NULL;
				singleRegion.clear();
			}
		}
	}

	visited.clear();

	for (int i = 0; i < (int)candidates.size(); i++)
	{
		CBlobData *blob = new CBlobData(width, height);
		blob->m_IntensityRange = candidates[i]->intensitylevels;
		blob->m_Threshold = candidates[i]->threshold;
		blob->m_PixelCount = (int)candidates[i]->pixels->size();
		blob->m_Rect.left = width;
		blob->m_Rect.right = 0;
		blob->m_Rect.top = height;
		blob->m_Rect.bottom = 0;
		for (int j = 0; j < (int)candidates[i]->pixels->size(); j++)
		{
			blob->m_AverageIntensity += image[(int)(*candidates[i]->pixels)[j]];
			blob->m_Map[(int)(*candidates[i]->pixels)[j]] = (unsigned char)255;
			int x = ((int)(*candidates[i]->pixels)[j] % width);
			int y = ((int)(*candidates[i]->pixels)[j] / width);
			if (x < blob->m_Rect.left)
				blob->m_Rect.left = x;
			if (x > blob->m_Rect.right)
				blob->m_Rect.right = x;
			if (y < blob->m_Rect.top)
				blob->m_Rect.top = y;
			if (y > blob->m_Rect.bottom)
				blob->m_Rect.bottom = y;
			if (image[(int)(*candidates[i]->pixels)[j]] > blob->m_MaxIntensity)
			{
				blob->m_MaxIntensity = image[(int)(*candidates[i]->pixels)[j]];
				blob->m_MaxIntenX = x;
				blob->m_MaxIntenY = y;
			}
		}
		if (blob->m_PixelCount > 0)
			blob->m_AverageIntensity /= blob->m_PixelCount;
		blobList->push_back(blob);
		delete candidates[i];
		candidates[i] = NULL;
	}
	candidates.clear();
}