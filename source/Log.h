// Log.h: interface for the CLog class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include <afxmt.h>

class CLog  
{
	FILE *fp;
	CString dir;
	CString msg;
	CCriticalSection CS;

public:
	void NewLog(CString winCaption);
	void Message(CString str);
	CLog();
	virtual ~CLog();
};

