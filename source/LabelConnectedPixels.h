#pragma once
#include <stack>

using namespace std;

class CLabelConnectedPixels
{
protected:
	int m_Width;
	int m_Height;
	unsigned short *m_Image;
	stack <unsigned int> m_Stack;
	unsigned short m_Threshold;
	unsigned short *m_Map;

public:
	CLabelConnectedPixels();
	virtual ~CLabelConnectedPixels();

	void LabelConnectedComponents(unsigned short *image, int width, int height, int threshold, unsigned short *map, unsigned short *lastLabel);
	void DoLabeling();
};

