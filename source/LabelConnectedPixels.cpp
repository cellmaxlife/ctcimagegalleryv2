#include "stdafx.h"
#include <iostream>
#include "LabelConnectedPixels.h"

CLabelConnectedPixels::CLabelConnectedPixels()
{
}

CLabelConnectedPixels::~CLabelConnectedPixels()
{
	while (!m_Stack.empty())
	{
		m_Stack.pop();
	}
}

void CLabelConnectedPixels::LabelConnectedComponents(unsigned short *image, int width, int height, int threshold, unsigned short *map, unsigned short *lastLabel)
{
	*lastLabel = 0;
	while (!m_Stack.empty())
	{
		m_Stack.pop();
	}
	m_Threshold = (unsigned short)threshold;
	m_Width = width;
	m_Height = height;
	m_Image = image;
	m_Map = map;
	
	unsigned short currentLabel = (unsigned short)0;

	unsigned short *ptrImage = image;
	unsigned short *ptrMap = map;

	try
	{
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++, ptrImage++, ptrMap++)
			{
				if ((*ptrMap == (unsigned short)0) && (*ptrImage >= m_Threshold))
				{
					if (currentLabel == (unsigned char)MAXUINT16)
						throw MAXUINT16;
					currentLabel++;
					*ptrMap = currentLabel;
					unsigned short xpos = (unsigned short)j;
					unsigned short ypos = (unsigned short)i;
					unsigned int pos = (unsigned int)ypos;
					pos = (pos << 16) | xpos;
					m_Stack.push(pos);
					DoLabeling();
				}
			}
		}
		*lastLabel = currentLabel;
	}
	catch (...)
	{
		*lastLabel = currentLabel;
	}
}

void CLabelConnectedPixels::DoLabeling()
{
	while (!m_Stack.empty())
	{
		unsigned int pos = m_Stack.top();
		m_Stack.pop();
		int x0 = (int)(pos & 0xFFFF);
		int y0 = (int)((pos >> 16) & 0xFFFF);
		unsigned short currentLabel = m_Map[m_Width * y0 + x0];
		int x1 = 0;
		int y1 = 0;
		for (int i = 0; i < 8; i++)
		{
			switch (i)
			{
			case 0:
				x1 = x0 - 1;
				y1 = y0 - 1;
				break;
			case 1:
				x1 = x0;
				y1 = y0 - 1;
				break;
			case 2:
				x1 = x0 + 1;
				y1 = y0 - 1;
				break;
			case 3:
				x1 = x0 + 1;
				y1 = y0;
				break;
			case 4:
				x1 = x0 + 1;
				y1 = y0 + 1;
				break;
			case 5:
				x1 = x0;
				y1 = y0 + 1;
				break;
			case 6:
				x1 = x0 - 1;
				y1 = y0 + 1;
				break;
			case 7:
				x1 = x0 - 1;
				y1 = y0;
				break;
			}

			if ((x1 >= 0) && (x1 < m_Width) && (y1 >= 0) && (y1 < m_Height))
			{
				int index = m_Width * y1 + x1;
				if ((m_Map[index] == (unsigned short)0) && (m_Image[index] >= m_Threshold))
				{
					m_Map[index] = currentLabel;
					unsigned short xpos = (unsigned short)x1;
					unsigned short ypos = (unsigned short)y1;
					unsigned int pos = (unsigned int)ypos;
					pos = (pos << 16) | xpos;
					m_Stack.push(pos);
				}
			}
		}
	}
}

