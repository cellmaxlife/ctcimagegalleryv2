//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 產生的 Include 檔案。
// 由 CTCImageGallery.rc 使用
//
#define IDD_CTCIMAGEGALLERY_DIALOG      102
#define IDR_MAINFRAME                   128
#define IDC_LOADIMG                     1000
#define IDC_LOADRGN                     1001
#define IDC_NEXTVIEW                    1002
#define IDC_PREVVIEW                    1003
#define IDC_SAVERGN                     1004
#define IDC_EDIT1                       1005
#define IDC_EDIT2                       1006
#define IDC_EDIT3                       1007
#define IDC_TOTALREGIONS                1007
#define IDC_IMAGE1                      1008
#define IDC_IMAGE2                      1009
#define IDC_IMAGE                       1009
#define IDC_IMAGE3                      1010
#define IDC_HITSLOADED                  1010
#define IDC_IMAGE4                      1011
#define IDC_EDIT5                       1011
#define IDC_HITSDISPLAYED               1011
#define IDC_TOTALPAGES                  1011
#define IDC_IMAGE5                      1012
#define IDC_EDIT4                       1012
#define IDC_HITCOUNT                    1012
#define IDC_TOTALCTCS                   1012
#define IDC_IMAGE6                      1013
#define IDC_BUTTON1                     1013
#define IDC_GOTO                        1013
#define IDC_IMAGE7                      1014
#define IDC_PAGENUM                     1014
#define IDC_IMAGE8                      1015
#define IDC_MANUALRGN                   1015
#define IDC_IMAGE9                      1016
#define IDC_MANUALTOTAL                 1016
#define IDC_IMAGE10                     1017
#define IDC_EDIT8                       1017
#define IDC_UNMATCHED                   1017
#define IDC_IMAGE11                     1018
#define IDC_REDCOLOR                    1018
#define IDC_IMAGE12                     1019
#define IDC_GREENCOLOR                  1019
#define IDC_IMAGE13                     1020
#define IDC_BLUECOLOR                   1020
#define IDC_SHOW                        1021
#define IDC_IMAGE14                     1022
#define IDC_RADIO2                      1022
#define IDC_NOTSHOW                     1022
#define IDC_IMAGE15                     1023
#define IDC_MANUALMATCHED               1023
#define IDC_IMAGE16                     1024
#define IDC_AUTOUNMATCHED               1024
#define IDC_IMAGE17                     1025
#define IDC_EDIT9                       1025
#define IDC_AUTOMATCHED                 1025
#define IDC_IMAGE18                     1026
#define IDC_IMAGE19                     1027
#define IDC_IMAGE20                     1028
#define IDC_IMAGE21                     1029
#define IDC_IMAGE22                     1030
#define IDC_IMAGE23                     1031
#define IDC_IMAGE24                     1032
#define IDC_IMAGE25                     1033
#define IDC_IMAGE26                     1034
#define IDC_IMAGE27                     1035

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
