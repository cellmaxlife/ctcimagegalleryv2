﻿
// CTCImageGalleryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTCImageGallery.h"
#include "CTCImageGalleryDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define IMAGE_WINDOW_X0 13
#define IMAGE_WINDOW_Y0 56
#define IMAGE_WINDOW_X1 1704
#define IMAGE_WINDOW_Y1 987

#define SAMPLEFRAMEWIDTH  100
#define SAMPLEFRAMEHEIGHT 100
#define SAMPLEFRAMEWIDTH2  50
#define SAMPLEFRAMEHEIGHT2 50


// CCTCImageGalleryDlg dialog

CCTCImageGalleryDlg::CCTCImageGalleryDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCTCImageGalleryDlg::IDD, pParent)
	, m_ImageFilename(_T(""))
	, m_RegionFilename(_T(""))
	, m_TotalRegions(0)
	, m_TotalPages(0)
	, m_TotalSelected(0)
	, m_CurrentPage(0)
	, m_ManualRgnFilename(_T(""))
	, m_TotalManual(0)
	, m_TotalUnmatched(0)
	, m_AutoMatchedCount(0)
	, m_AutoUnmatchedCount(0)
	, m_ManualMatchedCount(0)
	, m_MatchedIndexList(NULL)
	, m_MatchedIndexListLength(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_RedIntensity = 200;
	m_GreenIntensity = 200;
	m_BlueIntensity = 200;
	m_ImageWidth = 100;
	m_ImageHeight = 100;
	m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	m_RedRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_GreenRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_BlueRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
}

void CCTCImageGalleryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_ImageFilename);
	DDX_Text(pDX, IDC_EDIT2, m_RegionFilename);
	DDX_Text(pDX, IDC_MANUALRGN, m_ManualRgnFilename);
	DDX_Text(pDX, IDC_TOTALCTCS, m_TotalSelected);
	DDX_Control(pDX, IDC_IMAGE, m_HitImage);
	DDX_Text(pDX, IDC_TOTALREGIONS, m_TotalRegions);
	DDX_Text(pDX, IDC_TOTALPAGES, m_TotalPages);
	DDX_Text(pDX, IDC_PAGENUM, m_CurrentPage);
	DDX_Text(pDX, IDC_MANUALTOTAL, m_TotalManual);
	DDX_Text(pDX, IDC_UNMATCHED, m_TotalUnmatched);
	DDX_Control(pDX, IDC_BLUECOLOR, m_BlueColor);
	DDX_Control(pDX, IDC_GREENCOLOR, m_GreenColor);
	DDX_Control(pDX, IDC_REDCOLOR, m_RedColor);
	DDX_Control(pDX, IDC_SHOW, m_ShowBoundary);
	DDX_Control(pDX, IDC_NOTSHOW, m_NotShowBoundary);
	DDX_Text(pDX, IDC_AUTOMATCHED, m_AutoMatchedCount);
	DDX_Text(pDX, IDC_AUTOUNMATCHED, m_AutoUnmatchedCount);
	DDX_Text(pDX, IDC_MANUALMATCHED, m_ManualMatchedCount);
}

BEGIN_MESSAGE_MAP(CCTCImageGalleryDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_LOADIMG, &CCTCImageGalleryDlg::OnBnClickedLoadimg)
	ON_BN_CLICKED(IDC_LOADRGN, &CCTCImageGalleryDlg::OnBnClickedLoadrgn)
	ON_BN_CLICKED(IDC_NEXTVIEW, &CCTCImageGalleryDlg::OnBnClickedNextview)
	ON_BN_CLICKED(IDC_SAVERGN, &CCTCImageGalleryDlg::OnBnClickedSavergn)
	ON_BN_CLICKED(IDCANCEL, &CCTCImageGalleryDlg::OnBnClickedCancel)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_PREVVIEW, &CCTCImageGalleryDlg::OnBnClickedPrevview)
	ON_BN_CLICKED(IDC_GOTO, &CCTCImageGalleryDlg::OnBnClickedGoto)
	ON_BN_CLICKED(IDC_REDCOLOR, &CCTCImageGalleryDlg::OnBnClickedRedcolor)
	ON_BN_CLICKED(IDC_GREENCOLOR, &CCTCImageGalleryDlg::OnBnClickedGreencolor)
	ON_BN_CLICKED(IDC_BLUECOLOR, &CCTCImageGalleryDlg::OnBnClickedBluecolor)
	ON_BN_CLICKED(IDC_SHOW, &CCTCImageGalleryDlg::OnBnClickedShow)
	ON_BN_CLICKED(IDC_NOTSHOW, &CCTCImageGalleryDlg::OnBnClickedNotshow)
END_MESSAGE_MAP()


// CCTCImageGalleryDlg message handlers

BOOL CCTCImageGalleryDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("CTCImageGallery(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_RedColor.SetCheck(BST_CHECKED);
	m_GreenColor.SetCheck(BST_CHECKED);
	m_BlueColor.SetCheck(BST_CHECKED);
	m_NotShowBoundary.SetCheck(BST_UNCHECKED);
	m_ShowBoundary.SetCheck(BST_CHECKED);
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_GOTO);
	btn->EnableWindow(FALSE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCTCImageGalleryDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintCImages();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCTCImageGalleryDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCTCImageGalleryDlg::UpdateImageDisplay()
{
	RECT rect;
	m_HitImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}


void CCTCImageGalleryDlg::OnBnClickedLoadimg()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(FALSE); 
	btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_GOTO);
	btn->EnableWindow(FALSE);
	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString LEICA_GREEN_FILEPREFIX = _T("FITC Selection");
	const CString LEICA_BLUE_FILEPREFIX = _T("DAPI Selection");
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString ZEISS_GREEN_POSTFIX = _T("_c4_ORG.tif");
	const CString ZEISS_BLUE_POSTFIX = _T("_c1_ORG.tif");
	FreeRGNList(&m_AutoRGNDataList);
	FreeRGNList(&m_ManualRGNDataList);
	FreeMatchedIndexList();
	m_AutoUnmatchedIndexList.clear();
	m_ManualUnmatchedIndexList.clear();
	m_ManualMatchedIndexList.clear();
	m_DisplayedMatchIndexList.clear();
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	ResetAllCounts();
	m_RegionFilename = _T("");
	m_ManualRgnFilename = _T("");
	UpdateData(FALSE);
	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_ImageFilename = dlg.GetFileName();
		UpdateData(FALSE);
		int index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
		if (index == -1)
		{
			index = m_ImageFilename.Find(ZEISS_RED_POSTFIX, 0);
		}
		if (index == -1)
		{
			message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
			AfxMessageBox(message);
			return;
		}
		bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
		if (!ret)
		{	
			message.Format(_T("Failed to load %s"), m_ImageFilename);
			AfxMessageBox(message);
			return;
		}
		else
		{
			message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
			m_Log.Message(message);
			index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
			if (index == -1)
			{
				int postfixIndex = filename.Find(ZEISS_RED_POSTFIX);
				if (postfixIndex == -1)
				{
					message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
					AfxMessageBox(message);
					return;
				}
				else
				{
					CString samplePathName = filename.Mid(0, postfixIndex);
					postfixIndex = m_ImageFilename.Find(ZEISS_RED_POSTFIX);
					CString sampleName = m_ImageFilename.Mid(0, postfixIndex);
					CString filename2 = samplePathName + ZEISS_GREEN_POSTFIX;
					BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename, m_GreenTIFFData->GetCPI());
						m_Log.Message(message);
						filename2 = samplePathName + ZEISS_BLUE_POSTFIX;
						BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							message.Format(_T("Blue TIFF File %s loaded successfully, Green CPI=%d"), filename, m_BlueTIFFData->GetCPI());
							m_Log.Message(message);
							message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
							m_CTCParams.LoadCTCParams(message);
							btn = (CButton *)GetDlgItem(IDC_LOADRGN);
							btn->EnableWindow(TRUE);
						}
						else
						{
							message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
							AfxMessageBox(message);
							return;
						}
					}
					else
					{
						message.Format(_T("Failed to load Green TIFF File %s"), filename2);
						AfxMessageBox(message);
						return;
					}
				}
			}
			else
			{
				CString postfix = m_ImageFilename.Mid(LEICA_RED_FILEPREFIX.GetLength());
				index = filename.Find(LEICA_RED_FILEPREFIX, 0);
				CString pathname = filename.Mid(0, index);
				CString filename2;
				filename2.Format(_T("%s%s%s"), pathname, LEICA_GREEN_FILEPREFIX, postfix);
				ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Green TIFF File %s"), filename2);
					AfxMessageBox(message);
					return;
				}
				else
				{
					message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
					m_Log.Message(message);
					pathname = filename.Mid(0, index);
					filename2.Format(_T("%s%s%s"), pathname, LEICA_BLUE_FILEPREFIX, postfix);
					ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
						AfxMessageBox(message);
						return;
					}
					else
					{
						message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
						m_Log.Message(message);
						message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
						m_CTCParams.LoadCTCParams(message);
						btn = (CButton *)GetDlgItem(IDC_LOADRGN);
						btn->EnableWindow(TRUE);
					}
				}
			}
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(TRUE);
}

void CCTCImageGalleryDlg::OnBnClickedLoadrgn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_GOTO);
	btn->EnableWindow(FALSE);
	FreeRGNList(&m_AutoRGNDataList);
	FreeRGNList(&m_ManualRGNDataList);
	FreeMatchedIndexList();
	m_AutoUnmatchedIndexList.clear();
	m_ManualUnmatchedIndexList.clear();
	m_ManualMatchedIndexList.clear();
	m_DisplayedMatchIndexList.clear();
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	ResetAllCounts();
	m_RegionFilename = _T("");
	m_ManualRgnFilename = _T("");
	UpdateData(FALSE);
	m_RedIntensity = m_RedTIFFData->GetCPI();
	m_GreenIntensity = m_GreenTIFFData->GetCPI();
	m_BlueIntensity = m_BlueTIFFData->GetCPI();
	CString message;
	for (int i = 0; i <= (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), m_CTCParams.m_CTCParamNames[i], m_CTCParams.m_CTCParams[i]);
		m_Log.Message(message);
	}

	CFileDialog dlg1(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Manual Region files (*.rgn)|*.rgn"), NULL, 0, TRUE);
	if (dlg1.DoModal() == IDOK)
	{
		CString filename = dlg1.GetPathName();
		m_ManualRgnFilename = dlg1.GetFileName();
		UpdateData(FALSE);
		if (LoadRegionData(filename, false))
		{
			message.Format(_T("Manual Region File %s loaded successfully, #Hits=%d"), filename, m_ManualRGNDataList.size());
			m_Log.Message(message);
			m_TotalManual = (int) m_ManualRGNDataList.size();
			m_MatchedIndexListLength = m_TotalManual;
			m_MatchedIndexList = new vector<int> *[m_MatchedIndexListLength];
			memset(m_MatchedIndexList, 0, sizeof(vector<int> *) * m_MatchedIndexListLength);
			UpdateData(FALSE);
		}
		else
		{
			message.Format(_T("Failed to load Manual Region File %s"), filename);
			m_Log.Message(message);
			message.Format(_T("Failed to load Manual Region File %s"), m_ManualRgnFilename);
			AfxMessageBox(message);
			btn = (CButton *)GetDlgItem(IDC_LOADRGN);
			btn->EnableWindow(TRUE);
			btn = (CButton *)GetDlgItem(IDC_LOADIMG);
			btn->EnableWindow(TRUE);
			return;
		}
	}

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Auto Region files (*.rgn, *.tmp)|*.rgn; *.tmp"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_RegionFilename = dlg.GetFileName();
		UpdateData(FALSE);
		if (LoadRegionData(filename, true))
		{
			message.Format(_T("Region File %s loaded successfully, #Hits=%d"), filename, m_AutoRGNDataList.size());
			m_Log.Message(message);
			m_TotalRegions = (int) m_AutoRGNDataList.size();
			for (int i = 0; i < (int)m_AutoRGNDataList.size(); i++)
			{
				int x0, y0;
				m_AutoRGNDataList[i]->GetPosition(&x0, &y0);
				bool matched = false;
				for (int j = 0; j < m_TotalManual; j++)
				{
					int x1, y1;
					m_ManualRGNDataList[j]->GetPosition(&x1, &y1);
					if ((abs(x0 - x1) < SAMPLEFRAMEWIDTH2) && (abs(y0 - y1) < SAMPLEFRAMEHEIGHT2))
					{
						matched = true;
						if (m_MatchedIndexList[j] == NULL)
						{
							m_MatchedIndexList[j] = new vector<int>();
							m_MatchedIndexList[j]->push_back(i);
							m_ManualMatchedIndexList.push_back(j);
							m_DisplayedMatchIndexList.push_back(-1);
						}
						else
							m_MatchedIndexList[j]->push_back(i);
					}
				}
				if (matched)
				{
					m_AutoMatchedCount++;
				}
				else
				{
					m_AutoUnmatchedIndexList.push_back(i);
					m_AutoUnmatchedCount++;
				}
			}
			for (int i = 0; i < m_MatchedIndexListLength; i++)
			{
				if (m_MatchedIndexList[i] == NULL)
				{
					m_ManualUnmatchedIndexList.push_back(i);
					m_TotalUnmatched++;
				}
				else
					m_ManualMatchedCount++;
			}
			if (m_TotalRegions > 0)
			{
				int numRegionsToDisplay = m_AutoUnmatchedCount + m_TotalUnmatched + m_ManualMatchedCount;
				m_TotalPages = (numRegionsToDisplay / NUM_IMAGES);
				m_TotalPages++;
				if ((numRegionsToDisplay % NUM_IMAGES) == 0)
					m_TotalPages++;
				if (m_TotalPages >= 1)
					m_CurrentPage = 1;
				UpdateData(FALSE);
				DisplayCellRegions();
				btn = (CButton *)GetDlgItem(IDC_SAVERGN);
				btn->EnableWindow(TRUE);
			}
			message.Format(_T("TotalPages=%d, CurrentPage=%d"), m_TotalPages, m_CurrentPage);
			m_Log.Message(message);
			if (m_CurrentPage < m_TotalPages)
			{
				btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
				btn->EnableWindow(TRUE);
				btn = (CButton *)GetDlgItem(IDC_GOTO);
				btn->EnableWindow(TRUE);
			}
		}
		else
		{
			message.Format(_T("Failed to load Auto Region File %s"), m_RegionFilename);
			AfxMessageBox(message);
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(TRUE);
}


void CCTCImageGalleryDlg::OnBnClickedNextview()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	if (m_CurrentPage < m_TotalPages)
	{
		SaveTempRegionFile();
		m_CurrentPage++;
		UpdateData(FALSE);
		DisplayCellRegions();
		btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
		btn->EnableWindow(TRUE);
	}
	if (m_CurrentPage < m_TotalPages)
	{
		btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
		btn->EnableWindow(TRUE);
	}
}

void CCTCImageGalleryDlg::OnBnClickedPrevview()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
	btn->EnableWindow(FALSE);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	if (m_CurrentPage > 1)
	{
		SaveTempRegionFile();
		m_CurrentPage--;
		UpdateData(FALSE);
		DisplayCellRegions();
		btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
		btn->EnableWindow(TRUE);
	}
	if (m_CurrentPage > 1)
	{
		btn = (CButton *)GetDlgItem(IDC_PREVVIEW);
		btn->EnableWindow(TRUE);
	}
}

void CCTCImageGalleryDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_TotalSelected > 0)
	{
		CString filenameForSave;

		CString filename = m_RegionFilename;
		WCHAR *char1 = _T(".");
		int fileStringIndex = filename.ReverseFind(*char1);
		filename = filename.Mid(0, fileStringIndex);
		filename += _T("_Selection.rgn");
		
		CFileDialog dlg(FALSE,    // save
			CString(".rgn"),    
			filename,    
			OFN_OVERWRITEPROMPT,
			_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			filenameForSave = dlg.GetPathName();
			m_RegionFilename = dlg.GetFileName();
			UpdateData(FALSE);

			CStdioFile theFile;
			
			if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
			{
				int count = 0;
				for (int i = 0; i < (int)m_AutoUnmatchedIndexList.size(); i++)
				{
					if (m_AutoRGNDataList[m_AutoUnmatchedIndexList[i]]->GetColorCode() == CTC)
					{
						count++;
						CString singleLine;
						int x0, y0;
						m_AutoRGNDataList[m_AutoUnmatchedIndexList[i]]->GetPosition(&x0, &y0);
						singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
							m_AutoRGNDataList[m_AutoUnmatchedIndexList[i]]->GetColorCode(), x0, y0, count);
						theFile.WriteString(singleLine);
					}
				}
				for (int i = 0; i < (int)m_ManualUnmatchedIndexList.size(); i++)
				{
					if (m_ManualRGNDataList[m_ManualUnmatchedIndexList[i]]->GetColorCode() == CTC2)
					{
						count++;
						CString singleLine;
						int x0, y0;
						m_ManualRGNDataList[m_ManualUnmatchedIndexList[i]]->GetPosition(&x0, &y0);
						singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
							m_ManualRGNDataList[m_ManualUnmatchedIndexList[i]]->GetColorCode(), x0, y0, count);
						theFile.WriteString(singleLine);
					}
				}
				for (int i = 0; i < m_ManualMatchedCount; i++)
				{
					for (int j = 0; j < (int)m_MatchedIndexList[m_ManualMatchedIndexList[i]]->size(); j++)
					{
						int x0, y0;
						m_AutoRGNDataList[(*m_MatchedIndexList[m_ManualMatchedIndexList[i]])[j]]->GetPosition(&x0, &y0);
						bool found = false;
						for (int k = i + 1; k < m_ManualMatchedCount; k++)
						{
							for (int h = 0; h < m_MatchedIndexList[m_ManualMatchedIndexList[k]]->size(); h++)
							{
								int x1, y1;
								m_AutoRGNDataList[(*m_MatchedIndexList[m_ManualMatchedIndexList[k]])[h]]->GetPosition(&x1, &y1);
								if ((x1 == x0) && (y1 == y0))
								{
									found = true;
									break;
								}
							}
							if (found)
								break;
						}
						if (!found)
						{
							count++;
							CString singleLine;
							singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
								m_AutoRGNDataList[(*m_MatchedIndexList[m_ManualMatchedIndexList[i]])[j]]->GetColorCode(), x0, y0, count);
							theFile.WriteString(singleLine);
						}
					}
				}
				theFile.Close();
			}
		}
	}
}

void  CCTCImageGalleryDlg::PaintCImages()
{

	CPaintDC dc(&m_HitImage);
	CRect rect;
	m_HitImage.GetClientRect(&rect);
	dc.SetStretchBltMode(HALFTONE);
	m_Image.StretchBlt(dc.m_hDC, rect);
	CDC* pDC = m_HitImage.GetDC();
	CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
	CPen *pOldPen = pDC->SelectObject(&CursorPen);
	int rectWidth = rect.right - rect.left + 1;
	int rectHeight = rect.bottom - rect.top + 1;
	int blockWidth = rectWidth / NUM_IMAGE_COLUMNS;
	int blockHeight = rectHeight / NUM_IMAGE_ROWS;
	for (int i = 1; i < NUM_IMAGE_ROWS; i++)
	{
		pDC->MoveTo(rect.left, rect.top + i * blockHeight);
		pDC->LineTo(rect.right, rect.top + i * blockHeight);
	}
	for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
	{
		pDC->MoveTo(rect.left + i * blockWidth, rect.top);
		pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
	}
	CPen CursorPen1(PS_SOLID, 5, RGB(255, 0, 0));
	pDC->SelectObject(&CursorPen1);
	if ((m_CurrentPage >= 1) && (m_CurrentPage <= m_TotalPages))
	{
		int regionIndex = (m_CurrentPage - 1) * NUM_IMAGES;
		int totalSelectables = m_AutoUnmatchedCount;
		int totalManual = totalSelectables + m_TotalUnmatched;
		int totalRegions = totalManual + m_ManualMatchedCount;
		for (int i = 0; i < NUM_IMAGES; i++, regionIndex++)
		{
			int ii = i / NUM_IMAGE_COLUMNS;
			int jj = i % NUM_IMAGE_COLUMNS;	
			CRect rect1;
			rect1.left = rect.left + jj * blockWidth + 10;
			rect1.top = rect.top + ii * blockHeight + 10;
			rect1.right = rect1.left + 200;
			rect1.bottom = rect1.top + 20;
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextAlign(TA_LEFT);
			CString label;
			if ((regionIndex >= 0) && (regionIndex < totalSelectables))
			{
				int index = m_AutoUnmatchedIndexList[regionIndex];
				label.Format(_T("A%d"), index+1);
				if (m_AutoRGNDataList[index]->GetColorCode() == CTC)
				{
					pDC->SetTextColor(RGB(255, 0, 0));
				}					
			}
			else if ((regionIndex >= totalSelectables) && (regionIndex < totalManual))
			{
				int index = m_ManualUnmatchedIndexList[regionIndex - totalSelectables];
				label.Format(_T("M%d"), index+1);
			}
			else if ((regionIndex >= totalManual) && (regionIndex < totalRegions))
			{
				CString label1;
				int index = m_ManualMatchedIndexList[regionIndex - totalManual];
				label.Format(_T("M%d"), index+1);
				for (int j = 0; j < (int)m_MatchedIndexList[m_ManualMatchedIndexList[regionIndex - totalManual]]->size(); j++)
				{
					index = (*m_MatchedIndexList[m_ManualMatchedIndexList[regionIndex - totalManual]])[j];
					label1.Format(_T(",A%d"), index + 1);
					label += label1;
				}
				if (m_DisplayedMatchIndexList[regionIndex - totalManual] == -1)
				{
					index = m_ManualMatchedIndexList[regionIndex - totalManual];
					label1.Format(_T(",(M%d)"), index + 1);
				}
				else
				{
					index = (*m_MatchedIndexList[m_ManualMatchedIndexList[regionIndex - totalManual]])[m_DisplayedMatchIndexList[regionIndex - totalManual]];
					label1.Format(_T(",(A%d)"), index + 1);
				}
				label += label1;
			}
			pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);

			bool drawMarker = false;
			if ((regionIndex >= totalSelectables) && (regionIndex < totalManual))
			{
				drawMarker = true;
				if (m_ManualRGNDataList[m_ManualUnmatchedIndexList[regionIndex - totalSelectables]]->GetColorCode() == CTC2)
				{
					pDC->SetTextColor(RGB(255, 0, 0));
					label = _T("C");
				}
				else
				{
					pDC->SetTextColor(RGB(0, 255, 0));
					label = _T("W");
				}
			}
			else if ((regionIndex >= totalManual) && (regionIndex < totalRegions))
			{
				drawMarker = true;
				if (m_ManualRGNDataList[m_ManualMatchedIndexList[regionIndex - totalManual]]->GetColorCode() == CTC2)
				{
					pDC->SetTextColor(RGB(255, 0, 0));
					label = _T("C");
				}
				else
				{
					pDC->SetTextColor(RGB(0, 255, 0));
					label = _T("W");
				}
			}
			if (drawMarker)
			{
				CRect rect2;
				rect2.left = rect.left + jj * blockWidth + 170;
				rect2.top = rect.top + ii * blockHeight + 10;
				rect2.right = rect2.left + 20;
				rect2.bottom = rect2.top + 20;
				pDC->DrawTextW(label, &rect2, DT_SINGLELINE | DT_NOCLIP);
			}

			if ((regionIndex >= 0) && (regionIndex < totalSelectables))
			{
				if (m_AutoRGNDataList[m_AutoUnmatchedIndexList[regionIndex]]->GetColorCode() == CTC)
				{
					int x0 = rect.left + jj * blockWidth + 140;
					int y0 = rect.top + ii * blockHeight + 40;
					pDC->MoveTo(x0, y0);
					pDC->LineTo(x0 + 10, y0 + 20);
					pDC->LineTo(x0 + 30, y0 - 30);
				}
			}
		}
	}

	pDC->SelectObject(pOldPen);
}

void CCTCImageGalleryDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_AutoRGNDataList);
	FreeRGNList(&m_ManualRGNDataList);
	FreeMatchedIndexList();
	m_AutoUnmatchedIndexList.clear();
	m_ManualUnmatchedIndexList.clear();
	m_ManualMatchedIndexList.clear();
	m_DisplayedMatchIndexList.clear();
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	delete m_RedRegionImage;
	delete m_GreenRegionImage;
	delete m_BlueRegionImage;
	CDialogEx::OnCancel();
}

bool CCTCImageGalleryDlg::LoadRegionData(CString filename, bool isAuto)
{
	bool ret = false;
	const CString TAG1 = _T("0 1, 1 ");
	const CString TAG2 = _T(", 2 ");
	const CString TAG3 = _T(", ");

	vector<CRGNData *> *list = NULL;
	if (isAuto)
	{
		list = &m_AutoRGNDataList;
	}
	else
	{
		list = &m_ManualRGNDataList;
	}
	FreeRGNList(list);

	CStdioFile theFile;
	bool isTempFile = false;
	if (filename.Find(_T(".tmp")) > -1)
		isTempFile = true;

	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			int index = textline.Find(TAG1);
			if (index > -1)
			{
				textline = textline.Mid(TAG1.GetLength());
				index = textline.Find(TAG2);
				if (index > -1)
				{
					unsigned int color = _wtoi(textline.Mid(0, index));
					textline = textline.Mid(index + TAG2.GetLength());
					index = textline.Find(_T(" "));
					if (index > -1)
					{
						int x0 = _wtoi(textline.Mid(0, index));
						textline = textline.Mid(index + 1);
						index = textline.Find(TAG3);
						if (index > -1)
						{
							ret = true;
							int y0 = _wtoi(textline.Mid(0, index));
							CRGNData *data = new CRGNData(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT);
							if (isAuto)
							{
								if (!isTempFile)
									data->SetColorCode(NONCTC);
								else
									data->SetColorCode(color);
							}
							else
							{
								if ((color == CTC) || (color == CTC2))
									data->SetColorCode(CTC2);
								else
									data->SetColorCode(NONCTC);
							}
							data->SetCPI(RED_COLOR, m_RedIntensity);
							data->SetCPI(GREEN_COLOR, m_GreenIntensity);
							data->SetCPI(BLUE_COLOR, m_BlueIntensity);
							data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
							data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
							data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
							data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
							data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
							data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
							data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
							data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
							data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
							list->push_back(data);
							continue;
						}
						else
						{
							ret = false;
							break;
						}
					}
					else
					{
						ret = false;
						break;
					}
				}
				else
				{
					ret = false;
					break;
				}
			}
			else
			{
				ret = false;
				break;
			}
		}
		theFile.Close();
	}
	return ret;
}

void CCTCImageGalleryDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CCTCImageGalleryDlg::DisplayCellRegions()
{
	if ((m_CurrentPage >= 1) && (m_CurrentPage <= m_TotalPages))
	{
		int regionIndex = (m_CurrentPage - 1) * NUM_IMAGES;
		int totalSelectables = m_AutoUnmatchedCount;
		int totalManual = totalSelectables + m_TotalUnmatched;
		int totalRegions = totalManual + m_ManualMatchedCount;
		for (int i = 0; i < NUM_IMAGES; i++, regionIndex++)
		{
			if ((regionIndex >= 0) && (regionIndex < totalSelectables))
			{
				CRGNData *ptr = m_AutoRGNDataList[m_AutoUnmatchedIndexList[regionIndex]];
				int x0, y0;
				ptr->GetPosition(&x0, &y0);
				bool redRet = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
				bool greenRet = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
				bool blueRet = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
				if (redRet && greenRet && blueRet)
				{
					ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					ProcessOneRegionForDisplay(ptr);
					CopyToRGBImage(ptr, i);
					ptr->NullImages();
				}
				else
				{
					CString message;
					message.Format(_T("Failed to load region image: X0=%d,Y0=%d,Width=%d,Height=%d"),
						x0, y0, m_ImageWidth, m_ImageHeight);
					m_Log.Message(message);
					memset(m_RedRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_GreenRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_BlueRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					CRGNData *ptr1 = new CRGNData(0, 0, m_ImageWidth, m_ImageHeight);
					ptr1->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					CopyToRGBImage(ptr1, i);
					ptr1->NullImages();
					delete ptr1;
				}
			}
			else if ((regionIndex >= totalSelectables) && (regionIndex < totalManual))
			{
				CRGNData *ptr = m_ManualRGNDataList[m_ManualUnmatchedIndexList[regionIndex - totalSelectables]];
				int x0, y0;
				ptr->GetPosition(&x0, &y0);
				bool redRet = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
				bool greenRet = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
				bool blueRet = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
				if (redRet && greenRet && blueRet)
				{
					ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					ProcessOneRegionForDisplay(ptr);
					CopyToRGBImage(ptr, i);
					ptr->NullImages();
				}
				else
				{
					CString message;
					message.Format(_T("Failed to load region image: X0=%d,Y0=%d,Width=%d,Height=%d"),
						x0, y0, m_ImageWidth, m_ImageHeight);
					m_Log.Message(message);
					memset(m_RedRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_GreenRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_BlueRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					CRGNData *ptr1 = new CRGNData(0, 0, m_ImageWidth, m_ImageHeight);
					ptr1->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					CopyToRGBImage(ptr1, i);
					ptr1->NullImages();
					delete ptr1;
				}
			}
			else if ((regionIndex >= totalManual) && (regionIndex < totalRegions))
			{
				int count = (int)m_MatchedIndexList[m_ManualMatchedIndexList[regionIndex - totalManual]]->size() + 1;
				CRGNData **rgnDataArray = new CRGNData*[count];
				for (int j = 0; j<count; j++)
				{
					if (j == 0)
						rgnDataArray[j] = m_ManualRGNDataList[m_ManualMatchedIndexList[regionIndex - totalManual]];
					else
					{
						rgnDataArray[j] = m_AutoRGNDataList[(*m_MatchedIndexList[m_ManualMatchedIndexList[regionIndex - totalManual]])[j - 1]];
					}
				}
				GetBestFrameMax(rgnDataArray, count);
				CRGNData *ptr = m_ManualRGNDataList[m_ManualMatchedIndexList[regionIndex - totalManual]];
				if (m_DisplayedMatchIndexList[regionIndex - totalManual] > -1)
					ptr = m_AutoRGNDataList[(*m_MatchedIndexList[m_ManualMatchedIndexList[regionIndex - totalManual]])[m_DisplayedMatchIndexList[regionIndex - totalManual]]];
				int x0, y0;
				ptr->GetPosition(&x0, &y0);
				bool redRet = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
				bool greenRet = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
				bool blueRet = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
				if (redRet && greenRet && blueRet)
				{
					ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					CopyToRGBImage(ptr, i);
					ptr->NullImages();
				}
				else
				{
					CString message;
					message.Format(_T("Failed to load region image: X0=%d,Y0=%d,Width=%d,Height=%d"),
						x0, y0, m_ImageWidth, m_ImageHeight);
					m_Log.Message(message);
					memset(m_RedRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_GreenRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_BlueRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					CRGNData *ptr1 = new CRGNData(0, 0, m_ImageWidth, m_ImageHeight);
					ptr1->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					CopyToRGBImage(ptr1, i);
					ptr1->NullImages();
					delete ptr1;
				}
			}
			else
			{
				memset(m_RedRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				memset(m_GreenRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				memset(m_BlueRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				CRGNData *ptr1 = new CRGNData(0, 0, m_ImageWidth, m_ImageHeight);
				ptr1->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
				CopyToRGBImage(ptr1, i);
				ptr1->NullImages();
				delete ptr1;
			}
		}
	}
	else
	{
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
		}
	}
	UpdateImageDisplay();
}

void CCTCImageGalleryDlg::GetBestFrameMax(CRGNData **rgnArray, int count)
{
	int redMax = MAXINT;
	int greenMax = MAXINT;
	int blueMax = MAXINT;
	for (int i = 0; i < count; i++)
	{
		CRGNData *ptr = rgnArray[i];
		int x0, y0;
		ptr->GetPosition(&x0, &y0);
		bool redRet = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
		bool greenRet = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
		bool blueRet = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
		if (redRet && greenRet && blueRet)
		{
			ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
			ProcessOneRegionForDisplay(ptr);
			if ((ptr->m_RedFrameMax > 0) && (ptr->m_RedFrameMax < redMax))
				redMax = ptr->m_RedFrameMax;
			if ((ptr->m_GreenFrameMax > 0) && (ptr->m_GreenFrameMax < greenMax))
				greenMax = ptr->m_GreenFrameMax;
			if ((ptr->m_BlueFrameMax > 0) && (ptr->m_BlueFrameMax < blueMax))
				blueMax = ptr->m_BlueFrameMax;
			ptr->NullImages();
		}
	}

	for (int i = 0; i < count; i++)
	{
		CRGNData *ptr = rgnArray[i];
		if (i == 0)
		{
			if (redMax < (ptr->GetCPI(RED_COLOR) + ptr->GetCutoff(RED_COLOR)))
				redMax = ptr->GetCPI(RED_COLOR) + ptr->GetContrast(RED_COLOR);
			if (greenMax < (ptr->GetCPI(GREEN_COLOR) + ptr->GetCutoff(GREEN_COLOR)))
				greenMax = ptr->GetCPI(GREEN_COLOR) + ptr->GetContrast(GREEN_COLOR);
			if (blueMax < (ptr->GetCPI(BLUE_COLOR) + ptr->GetCutoff(BLUE_COLOR)))
				redMax = ptr->GetCPI(BLUE_COLOR) + ptr->GetContrast(BLUE_COLOR);
		}
		ptr->m_RedFrameMax = redMax;
		ptr->m_GreenFrameMax = greenMax;
		ptr->m_BlueFrameMax = blueMax;
	}
}

void CCTCImageGalleryDlg::CopyToRGBImage(CRGNData *rgnPtr, int ImageLocationIndex)
{
	BYTE *pCursor = (BYTE*)m_Image.GetBits();
	int nPitch = m_Image.GetPitch();

	unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
	unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
	unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
	int regionWidth = rgnPtr->GetWidth();
	int regionHeight = rgnPtr->GetHeight();

	int blueMax = rgnPtr->GetContrast(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR);
	if (rgnPtr->m_BlueFrameMax > 0)
		blueMax = rgnPtr->m_BlueFrameMax;
	int redMax = rgnPtr->GetContrast(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR);
	if (rgnPtr->m_RedFrameMax > 0)
		redMax = rgnPtr->m_RedFrameMax;

	BYTE bluePixelValue = 0;
	BYTE greenPixelValue = 0;
	BYTE redPixelValue = 0;

	for (int y = 0; y < regionHeight; y++)
	{
		for (int x = 0; x < regionWidth; x++)
		{
			bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
			greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, rgnPtr->GetContrast(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR), rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
			redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
			if (m_RedColor.GetCheck() == BST_UNCHECKED)
				redPixelValue = (BYTE)0;
			if (m_GreenColor.GetCheck() == BST_UNCHECKED)
				greenPixelValue = (BYTE)0;
			if (m_BlueColor.GetCheck() == BST_UNCHECKED)
				bluePixelValue = (BYTE)0;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
		}
	}

	if (m_ShowBoundary.GetCheck() == BST_CHECKED)
	{
		vector<CBlobData *> *redBlobs = rgnPtr->GetBlobData(RED_COLOR);

		if (redBlobs->size() > 0)
		{
			for (int i = 0; i < (int)redBlobs->size(); i++)
			{
				for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
				{
					unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
					int x0 = m_HitFinder.GetX(pos);
					int y0 = m_HitFinder.GetY(pos);
					pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
					pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
					pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
				}
			}
		}
	}
}

BYTE CCTCImageGalleryDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CCTCImageGalleryDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	int xLoc = -1;
	int xInterval = (IMAGE_WINDOW_X1 - IMAGE_WINDOW_X0) / NUM_IMAGE_COLUMNS;
	for (int i = 0; i < NUM_IMAGE_COLUMNS; i++)
	{
		if ((point.x >= (IMAGE_WINDOW_X0 + i * xInterval)) &&
			(point.x < (IMAGE_WINDOW_X0 + (i + 1) * xInterval)))
		{
			xLoc = i;
			break;
		}
	}
	int yLoc = -1;
	int yInterval = (IMAGE_WINDOW_Y1 - IMAGE_WINDOW_Y0) / NUM_IMAGE_ROWS;
	for (int i = 0; i < NUM_IMAGE_ROWS; i++)
	{
		if ((point.y >= (IMAGE_WINDOW_Y0 + i * yInterval)) &&
			(point.y < (IMAGE_WINDOW_Y0 + (i + 1) * yInterval)))
		{
			yLoc = i;
			break;
		}
	}
	if ((xLoc > -1) && (yLoc > -1))
	{
		int totalSelectables = m_AutoUnmatchedCount;
		int totalManual = totalSelectables + m_TotalUnmatched;
		int totalRegions = totalManual + m_ManualMatchedCount;
		int ImageLocationIndex = NUM_IMAGE_COLUMNS * yLoc + xLoc;
		int index = (m_CurrentPage - 1) * NUM_IMAGES + ImageLocationIndex;
		if ((index >= 0) && (index < totalRegions))
		{
			if ((index >= 0) && (index < totalSelectables))
			{
				if (m_AutoRGNDataList[m_AutoUnmatchedIndexList[index]]->GetColorCode() == CTC)
				{
					m_AutoRGNDataList[m_AutoUnmatchedIndexList[index]]->SetColorCode(NONCTC);
					m_TotalSelected--;
				}
				else
				{
					m_AutoRGNDataList[m_AutoUnmatchedIndexList[index]]->SetColorCode(CTC);
					m_TotalSelected++;
				}
				UpdateData(FALSE);
				UpdateImageDisplay();
			}
			else if ((index >= totalManual) && (index < totalRegions))
			{
				index -= totalManual;
				if (m_DisplayedMatchIndexList[index] < ((int)m_MatchedIndexList[m_ManualMatchedIndexList[index]]->size() - 1))
				{
					int index1 = m_DisplayedMatchIndexList[index];
					index1++;
					m_DisplayedMatchIndexList[index] = index1;
				}
				else
				{
					m_DisplayedMatchIndexList[index] = -1;
				}
				CRGNData *ptr = m_ManualRGNDataList[m_ManualMatchedIndexList[index]];
				if (m_DisplayedMatchIndexList[index] > -1)
					ptr = m_AutoRGNDataList[(*m_MatchedIndexList[m_ManualMatchedIndexList[index]])[m_DisplayedMatchIndexList[index]]];
				int x0, y0;
				ptr->GetPosition(&x0, &y0);
				bool redRet = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
				bool greenRet = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
				bool blueRet = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
				if (redRet && greenRet && blueRet)
				{
					ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					CopyToRGBImage(ptr, ImageLocationIndex);
					ptr->NullImages();
				}
				else
				{
					CString message;
					message.Format(_T("Failed to load region image: X0=%d,Y0=%d,Width=%d,Height=%d"),
						x0, y0, m_ImageWidth, m_ImageHeight);
					m_Log.Message(message);
					memset(m_RedRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_GreenRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					memset(m_BlueRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
					CRGNData *ptr1 = new CRGNData(0, 0, m_ImageWidth, m_ImageHeight);
					ptr1->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					CopyToRGBImage(ptr1, ImageLocationIndex);
					ptr1->NullImages();
					delete ptr1;
				}
				UpdateImageDisplay();
			}
			else
			{
				AfxMessageBox(_T("This marked region cannot be selected"));
			}
		}
	}
	CDialogEx::OnLButtonDblClk(nFlags, point);
}

void CCTCImageGalleryDlg::ProcessOneRegionForDisplay(CRGNData *ptr)
{
	unsigned int color = ptr->GetColorCode();
	vector<CRGNData *> cellList;
	m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
	if (cellList.size() > 0)
	{
		m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, NULL, 1);
		FreeRGNList(&cellList);
	}
	ptr->SetColorCode(color);
}

void CCTCImageGalleryDlg::OnBnClickedGoto()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if ((m_CurrentPage >= 1) && (m_CurrentPage <= m_TotalPages))
	{
		SaveTempRegionFile();
		DisplayCellRegions();
	}
}

BOOL CCTCImageGalleryDlg::PreTranslateMessage(MSG* pMsg)
{
	CEdit *pageNum = (CEdit *)GetDlgItem(IDC_PAGENUM);
	
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN) &&
		(GetFocus() == pageNum))
	{
		return TRUE; 
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CCTCImageGalleryDlg::OnBnClickedRedcolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedGreencolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedBluecolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedShow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}


void CCTCImageGalleryDlg::OnBnClickedNotshow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	DisplayCellRegions();
}

void CCTCImageGalleryDlg::ResetAllCounts()
{
	m_TotalPages = 0;
	m_TotalRegions = 0;
	m_TotalSelected = 0;
	m_CurrentPage = 0;
	m_TotalManual = 0;
	m_TotalUnmatched = 0;
	m_ManualMatchedCount = 0;
	m_AutoMatchedCount = 0;
	m_AutoUnmatchedCount = 0;
}

void CCTCImageGalleryDlg::FreeMatchedIndexList()
{
	if (m_MatchedIndexList != NULL)
	{
		for (int i = 0; i < m_MatchedIndexListLength; i++)
		{
			if (m_MatchedIndexList[i] != NULL)
			{
				m_MatchedIndexList[i]->clear();
				delete m_MatchedIndexList[i];
				m_MatchedIndexList[i] = NULL;
			}
		}
		delete[] m_MatchedIndexList;
		m_MatchedIndexList = NULL;
		m_MatchedIndexListLength = 0;
	}
}

void CCTCImageGalleryDlg::SaveRegionFile(CString filename)
{
	CStdioFile theFile;

	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		int count = 0;
		for (int i = 0; i < (int)m_AutoRGNDataList.size(); i++)
		{
			count++;
			CString singleLine;
			int x0, y0;
			m_AutoRGNDataList[i]->GetPosition(&x0, &y0);
			singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
				m_AutoRGNDataList[i]->GetColorCode(), x0, y0, count);
			theFile.WriteString(singleLine);
		}
		theFile.Close();
	}
}

void CCTCImageGalleryDlg::SaveTempRegionFile()
{
	CString filename = _T("C:\\CTCReviewerLog\\CTCImageGalleryV2Temp.tmp");
	SaveRegionFile(filename);
	CString message;
	message.Format(_T("CTCImageGalleryV3Temp.tmp file saved for %s at Page No.%d"), m_RegionFilename, m_CurrentPage);
	m_Log.Message(message);
}
